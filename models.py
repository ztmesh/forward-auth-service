from tortoise.models import Model
from tortoise import fields

class Entity(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=255)
    description = fields.TextField()
    type = fields.CharField(max_length=255)

    members = fields.ManyToManyField('models.Entity', related_name='member_of', through='entity_members')
    owners = fields.ManyToManyField('models.Entity', related_name='owner_of', through='entity_owners')

    member_of = fields.ReverseRelation["Entity"]
    owner_of = fields.ReverseRelation["Entity"]


    def __str__(self):
        return self.name