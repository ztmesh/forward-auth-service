let my_name = null;
(async function(){ 
    const profile = await fetch("/whoami").then(r => r.json());
    my_name = profile.name;
    prepareUsers();
})()

async function prepareUsers() {
    $("#users").hide()
    $("#entity").hide()
    $("#groups").hide()
    $("#loading").show()
    const users = await fetch("/entity?type=user").then(r => r.json());
    $("#users-table").html("")
    for (const user of users) {
        $("#users-table").append(`<tr><th>${user.id}</th><td><a href="javascript:showEntity(${user.id});">${user.name}</a>${user.name === my_name ? ' (you)' : ''}</td></tr>`)
    }
    $("#loading").hide()
    $("#users").show()
}

async function prepareGroups() {
    $("#users").hide()
    $("#entity").hide()
    $("#groups").hide()
    $("#loading").show()
    const users = await fetch("/entity?type=group").then(r => r.json());
    $("#groups-table").html("")
    for (const user of users) {
        $("#groups-table").append(`<tr><th>${user.id}</th><td><a href="javascript:showEntity(${user.id});">${user.name}</a>${user.name === my_name ? ' (you)' : ''}</td></tr>`)
    }
    $("#loading").hide()
    $("#groups").show()
}

async function showEntity(id) {
    $("#users").hide()
    $("#groups").hide()
    $("#entity").hide()
    $("#loading").show()
    const entity = await fetch(`/entity/${id}`).then(r => r.json());
    $("#entity-name").text(entity.name)
    $("#entity-description").text(entity.description ?? "");
    for (const params of [
        {items: entity.owners, html: "entity-owner"},
        {items: entity.members, html: "entity-member"},
        {items: entity.owner_of, html: "entity-owner-of"},
        {items: entity.member_of, html: "entity-member-of"},
    ]) {

        $(`#${params.html}-table`).html("");
        $(`#${params.html}`).hide()
        if (params.items.length) {
            for (const item of params.items) {
                $(`#${params.html}-table`).append(`<tr><th>${item.id}</th><td>${item.type}</td><td><a href="javascript:showEntity(${item.id});">${item.name}</a>${item.name === my_name ? ' (you)' : ''}</td></tr>`)
            }
            $(`#${params.html}`).show()
        }
    }
    $("#entity").show()
    $("#loading").hide()
}

