from tortoise import Tortoise, run_async
import os

# async def init():
#     # Here we create a SQLite DB using file "db.sqlite3"
#     #  also specify the app name of "models"
#     #  which contain models from "app.models"
#     await Tortoise.init(
#         db_url=os.environ.get("AUTH_MYSQL_URI"),
#         modules={'models': ['models']}
#     )
#     # Generate the schema
#     await Tortoise.generate_schemas()
# run_async(init())