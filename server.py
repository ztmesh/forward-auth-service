from fastapi import FastAPI, Request, Response, Header
from typing import Annotated
from tortoise.contrib.fastapi import register_tortoise
from models import Entity
import os, json
from cachetools import TTLCache
from asyncache import cached
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware

with open("./denied.html") as f:
    template = f.read()

# class Group(BaseModel):
#     name: str
#     description: str

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
# def error_response(code, data):
#     return Response(json.dumps(data), status_code=code, headers={'Content-Type': 'application/json'})

# def get_username_from_header(header):
#     if not header: return None

#     user_cn_parts = header.split("=")
#     if len(user_cn_parts) != 2: return None

#     return user_cn_parts[1]


# @cached(cache=TTLCache(maxsize=200, ttl=int(os.environ.get("CACHE_TTL", 30))))
# async def user_has_role(user_name, role):
#     if not isinstance(user_name, Entity):
#         entity = await Entity.filter(name=user_name).prefetch_related("member_of").first()
#         if not entity: return False, "User CN not found"
#     else:
#         entity = user_name
        
#     if not await entity.member_of.filter(name=role).count(): return False, f"You are not a member of '{role}'"

#     return True, entity

# async def get_request_entity(request: Request):
#     x_tls_client_subject = request.headers.get("x-tls-client-subject")
#     if not x_tls_client_subject and os.environ.get("APP_DEBUG"): x_tls_client_subject = "CN=" + os.environ.get("APP_DEBUG_USER")

#     user_name = get_username_from_header(x_tls_client_subject)
#     if not user_name: False, "User CN not provided or invalid"

#     entity = await Entity.filter(name=user_name).prefetch_related("member_of").first()
#     if not entity: return False, "User CN not found"

#     return True, entity

# async def request_has_role(request: Request, role):
#     x_tls_client_subject = request.headers.get("x-tls-client-subject")
#     if not x_tls_client_subject and os.environ.get("APP_DEBUG"): x_tls_client_subject = "CN=" + os.environ.get("APP_DEBUG_USER")

#     user_name = get_username_from_header(x_tls_client_subject)
#     if not user_name: return False, "User CN not provided or invalid"

#     return await user_has_role(user_name, role)

# @app.get("/")
# async def ui():
#     with open("./manager.html") as f:
#         return Response(f.read(), 200, {"Content-Type": "text/html"})
# @app.get("/manager.js")
# async def ui():
#     with open("./manager.js") as f:
#         return Response(f.read(), 200, {"Content-Type": "application/javascript"})

# @app.get("/entity")
# async def list_entities(type: str):
#     return await Entity.filter(type=type).all()

# @app.get("/whoami")
# async def whoami(request: Request):
#     entity = await get_request_entity(request)
#     if entity[0]: return entity[1]
#     else: return Response("Unknonw error", 500)
    
# @app.get("/entity/{entity_id}")
# async def get_entity(entity_id: int):
#     entity = await Entity.filter(id=entity_id).prefetch_related("member_of").prefetch_related("owner_of").first()
#     entity_dict = dict(entity)
#     entity_dict['member_of'] = await entity.member_of.all()
#     entity_dict['owner_of'] = await entity.owner_of.all()
#     entity_dict['members'] = await entity.members.all()
#     entity_dict['owners'] = await entity.owners.all()
#     return entity_dict

# @app.post("/group")
# async def create_group(group: Group, request: Request):
#     if await Entity.filter(name=group.name).count(): return error_response(400, "Group name already in use")
#     if not (await request_has_role(request, "AUTH_ROLE_CREATOR"))[0]: return error_response(403, "You do not have the 'AUTH_ROLE_CREATOR' role")
#     entity = await Entity.create(name=group.name, description=group.description, type="group")

#     request_entity = await get_request_entity(request)
#     await entity.owners.add(request_entity[1])
#     await entity.members.add(request_entity[1])

#     return {"created": True, "id": entity.id}

@app.get("/verify")
async def verify_caddy_request(request: Request):
    print(request.headers)
    return 'ok'
# async def verify_caddy_request(x_tls_client_subject: Annotated[str | None, Header()] = None, x_required_role: Annotated[str | None, Header()] = None):
    # user_name = get_username_from_header(x_tls_client_subject)
    # if not user_name: return Response(content=template.replace('%error%', f"The application was unable to verify your client certificate"), status_code=403, headers={"Content-Type": "text/html"})
    # if not x_required_role: return Response(content=template.replace('%error%', f"The application is misconfigured (MISSING_ROLE)"), status_code=403, headers={"Content-Type": "text/html"})

    # result, result_reason = await user_has_role(user_name, x_required_role)
    # if not result: return Response(content=template.replace('%error%', result_reason), status_code=403, headers={"Content-Type": "text/html"})

    # return Response(status_code=200, headers={"X-Auth-User-Id": str(result_reason.id)})

# register_tortoise(
#     app,
#     db_url=os.environ.get("AUTH_MYSQL_URI"),
#     modules={"models": ["models"]},
#     generate_schemas=True,
#     add_exception_handlers=True,
# )