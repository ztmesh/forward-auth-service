FROM python:3.11
WORKDIR /code

COPY ./requirements.txt /code/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt
COPY ./*.py ./*.js ./*.html /code/
CMD ["uvicorn", "server:app", "--host", "0.0.0.0", "--port", "3001"]
# CMD ["uvicorn", "server:app", "--host", "0.0.0.0", "--port", "3001", "--ssl-certfile", "/local/cert.pem", "--ssl-keyfile", "/local/key.pem", "--ssl-cert-reqs", "1", "--ssl-ca-certs", "/local/chain.pem"]